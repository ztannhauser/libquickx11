install: name = $(shell basename $(shell pwd))
install: main.so
	mkdir -p ~/libs ~/include
	cp main.so ~/libs/$(name).so
	find . -type f -name '*.h' | xargs ~/bin/makeheaders -h > ~/include/$(name).h

uninstall: name = $(shell basename $(shell pwd))
uninstall:
	rm ~/libs/$(name).so ~/include/$(name).h
