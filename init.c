
#include <X11/Xlib.h>
#include <X11/extensions/Xdbe.h>

#include "globals.h"

#define QUICK_EVENT_MASK                                               \
	(ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | \
	 ButtonReleaseMask | PointerMotionMask | StructureNotifyMask)

void quick_init() {
	display = XOpenDisplay(NULL);
	screen = DefaultScreen(display);
	gc = DefaultGC(display, screen);
	window = XCreateSimpleWindow(display, RootWindow(display, screen), 10, 10,
								 100, 100, 0, 0, 0);
	wm_delete_window = XInternAtom(display, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(display, window, &wm_delete_window, 1);
	XSelectInput(display, window, QUICK_EVENT_MASK);
	swapinfo.swap_window = window;
	swapinfo.swap_action = XdbeCopied;
	backbuffer = XdbeAllocateBackBufferName(display, window, XdbeCopied);
	XSetWindowAttributes winattrib = {
		.bit_gravity = NorthWestGravity  // CenterGravity
	};
	XChangeWindowAttributes(display, window, CWBitGravity, &winattrib);
}

