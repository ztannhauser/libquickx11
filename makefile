.PHONY: default
default: main.so

include arch.mk
include install.mk
include installdeps.mk
include .dir.mk

.%.c.o: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@
.%.cpp.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@
.dir.o:
	$(LD) -r $^ -o $@
%/.dir.o:
	$(LD) -r $^ -o $@

main: main.so

main.so: .dir.o
	$(CC) $(LDFLAGS) .dir.o $(LOADLIBES) $(LDLIBS) -o main.so
clean:
	find . -type f -name '*.o' | xargs rm -vf main
cleanmk:
	find . -type f -name '.*.mk' | xargs rm -vf
