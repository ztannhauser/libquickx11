/*/
/*/


void on_mouse_button_pressed(XButtonPressedEvent* event);

void on_mouse_button_released(XButtonReleasedEvent* event);

void on_key_pressed(XKeyPressedEvent* event);

void on_key_released(XKeyReleasedEvent* event);

void on_mouse_motion(XMotionEvent* event);

void on_redraw(XExposeEvent* event);

void on_configure_notify(XConfigureEvent* event);

void on_timeout(double delta);
