
#include <X11/Xlib.h>
#include <X11/extensions/Xdbe.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>

#include "globals.h"
#include "callbacks.h"

void quick_loop() {
	XMapWindow(display, window);
	XEvent event;
	clock_t then = clock(), now;
	for(bool keep_going = true; keep_going;) {
		while(XPending(display)) {
			XNextEvent(display, &event);
			switch(event.type) {
				case Expose: {
					on_redraw(&event);
					break;
				}
				case KeyPress: {
					on_key_pressed(&event);
					break;
				}
				case KeyRelease: {
					on_key_released(&event);
					break;
				}
				case ButtonPress: {
					on_mouse_button_pressed(&event);
					break;
				}
				case ButtonRelease: {
					on_mouse_button_released(&event);
					break;
				}
				case MotionNotify: {
					on_mouse_motion(&event);
					break;
				}
				case ConfigureNotify: {
					XConfigureEvent* spef = &event;
					width = spef->width;
					height = spef->height;
					on_configure_notify(spef);
					break;
				}
				case ClientMessage: {
					XClientMessageEvent* event_spef = &(event);
					if((Atom) event_spef->data.l[0] == wm_delete_window) {
						keep_going = false;
					}
					break;
				}
			}
		}
		now = clock();
		on_timeout((double) (now - then) / CLOCKS_PER_SEC);
		usleep(17000);
	}
	XUnmapWindow(display, window);
}

